.import QtQuick 2.0 as QQ

function getRandom(min, max) {
    var rand = Math.floor((Math.random() * max) + min);
    return rand;
}

function checkIfAllShooted() {
    if(aliveDucksAmount === 0)
         gameOverDialog.show(qsTr("Uzyskałeś " + pointsAmount + "/" + startDucksAmount + " punktów"))
}

function checkIfGone(x) {
    if (x < 0)
        lostDucks = lostDucks + 1
}

function checkIfAllGone() {
    print("alive = " + aliveDucksAmount)
    print("lostducks = " + lostDucks)

    if (lostDucks >= aliveDucksAmount)
        gameOverDialog.show(qsTr("Uzyskałeś " + pointsAmount + "/" + startDucksAmount + " punktów"))
}
