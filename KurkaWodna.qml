import QtQuick 2.6
import QtQuick.Controls 1.5
import QtQuick.Dialogs 1.2
import "Script.js" as Script

ApplicationWindow {
    id: root
    visible: true
    width: 640
    height: 480
    title: qsTr("Kurka Wodna")

    property int countdown: 0
    property int maxBulletsAmount: 10
    property int bulletsLeft: maxBulletsAmount
    property int pointsAmount: 0
    property int startDucksAmount: 10
    property int aliveDucksAmount: 2 * startDucksAmount
    property int lostDucks: 0

    signal bulletsOut()

    onAliveDucksAmountChanged: Script.checkIfAllShooted()

    onLostDucksChanged: {
        Script.checkIfAllGone()
    }

    onBulletsOut: {
        gameOverDialog.show(qsTr("Uzyskałeś " + pointsAmount + "/" + startDucksAmount + " punktów"))
    }

    onBulletsLeftChanged: {
        if (bulletsLeft === 1)
            root.bulletsOut()
    }

    MouseArea {
        id: gameArea
        anchors.fill: parent
        cursorShape: Qt.CrossCursor

        onClicked: root.bulletsLeft = (root.bulletsLeft - 1)
    }

    Image {
        id: backgroundImg
        anchors.fill: parent
        source: "background.png"
    }

    Repeater {
        id: ducksContainer
        model: startDucksAmount

        delegate: Image {
            id: duckImg
            source: "duck.png"

            property int yPos: Script.getRandom(0, root.height)
            property int xStart: Script.getRandom(0, 1000)
            property int velocity: Script.getRandom(10000, 10000)
            property int amplitude: Script.getRandom(5, 50)
            property int alive: 1

            width: 50
            height: 50

            NumberAnimation on x {
                from: (xStart + root.width);
                to: -width;
                duration: velocity
                onStopped: {
                    if (alive === 1) {
                        lostDucks = lostDucks + 1;
                    }
                }
            }

            SequentialAnimation on y {
                loops: Animation.Infinite
                NumberAnimation {
                    from: duckImg.yPos + duckImg.amplitude;
                    to: duckImg.yPos - duckImg.amplitude;
                    duration: 1600;
                    easing.type: Easing.InOutQuad
                }
                NumberAnimation {
                    from: duckImg.yPos - duckImg.amplitude;
                    to: duckImg.yPos + duckImg.amplitude;
                    duration: 1600;
                    easing.type: Easing.InOutQuad
                }
            }

            states:
                [
                State {
                    name: "alive"; when: (duckImg.alive == 1)
                    PropertyChanges { target: duckImg; visible: true; }
                },
                State {
                    name: "dead"; when: (duckImg.alive == 0)
                    PropertyChanges { target: duckImg; visible: false }
                }
            ]

            onStateChanged:  {
                root.aliveDucksAmount = (root.aliveDucksAmount - 1)
            }

            MouseArea {
                id: duckArea
                anchors.fill: parent
                cursorShape: Qt.CrossCursor

                onClicked: {
                    root.bulletsLeft = (root.bulletsLeft - 1)
                    root.pointsAmount = (root.pointsAmount + 1)
                    duckImg.alive = 0
                }
            }
        }
    }

    Image {
        id: cloudsImg
        source: "clouds.png"
        opacity: 0.8
        NumberAnimation on x { from: root.width; to: -root.width; duration: 15000; loops: Animation.Infinite}
    }

    Row {
        id: bulletsContainter
        x: (root.width - width)
        y: (root.height - height)

        Repeater {
            id: bulletsRepeater
            model: root.bulletsLeft

            delegate: Image {
                id: bulletImg
                source: "bullet.png"

                width: 50
                height: 50
            }

            onModelChanged: parent.children[0].visible = false
        }

        Text {
            id: pointsText
            color: "white"
            font.pointSize: 24
            text: qsTr("" + pointsAmount + " punktów")
            onTextChanged: text = qsTr("" + pointsAmount + " punktów")
        }
    }

    MessageDialog {
        id: gameOverDialog
        title: qsTr("Koniec gry")

        onAccepted: Qt.quit()

        function show(caption) {
            gameOverDialog.text = caption;
            gameOverDialog.open();
        }
    }
}
